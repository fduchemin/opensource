# Script de configuration post-installation pour les serveurs CentOS 7 

(c) Niki Kovacs, 2020

Ce référentiel fournit un script "automagique" de configuration
post-installation pour
des serveurs fonctionnant sous CentOS 7 ainsi qu'une collection de scripts
d'aide et
des modèles de fichiers de configuration pour les services communs.

## Dans l'interface système

Suivre les étapes ci-dessous.

  1. Installer CentOS 7 minimal.

  2. Creer un utilisateur non-'root' avec les privilèges administrateurs.

  3. Installer Git: `sudo yum install git`

  4. Recuperer le script: `git clone https://gitlab.com/kikinovak/centos-7.git`

  5. Se rendre dans le nouveau dossier: `cd centos-7`

  6. Exectuer le script: `sudo ./centos-setup.sh --setup`

  7. Maintenant allez prendre un café pendant que le script fait le travail !

  8. Redemarrer le système.


## Personnaliser un serveur CentOS.
Transformer une installation CentOS minimale en un serveur fonctionnel fini
toujours sur une série d'opérations plus ou moins longues.
Le temps et les choses a faires peuvent varier bien sûr, mais voici ce que je fais habituellement sur une nouvelle
installation CentOS :

  * Personnaliser l'interface de commande Bash : prompt, aliases, etc.

  * Personnaliser l'éditeur Vim.

  * Mettre en place les dépots officiels et les dépots tiers.

  * Installer un ensemble d'outils de ligne de commande.

  * Désinstaller de nombreux packages inutiles.

  * Activer l'utilisateur administrateur pour accéder aux logs du système.

  * Désactiver l'IPv6 et reconfigurer certains services en conséquence.
  
  * Configurer un mot de passe persistant pour `sudo`.

  * Etc.

Le script `centos-setup.sh` applique toutes ces opérations.

Configurer Bash et Vim puis applique une résolution plus lisible sur la
console:

```
# ./centos-setup.sh --shell
```

Configure les dépots officiels et tiers:

```
# ./centos-setup.sh --repos
```

Installe les packages `Core` et `Base` avec quelques outils supplémentaires:

```
# ./centos-setup.sh --extra
```

Désinstalle les packages inutiles:

```
# ./centos-setup.sh --prune
```

Active l'utilisateur administrateur pour accéder aux logs du système:

```
# ./centos-setup.sh --logs
```

Désactive l'IPv6 et reconfigure certains services en conséquence:

```
# ./centos-setup.sh --ipv4
```

Configurer un mot de passe persistent pour sudo:

```
# ./centos-setup.sh --sudo
```

Applique toutes les commandes ci-dessus en une seul fois:

```
# ./centos-setup.sh --setup
```

Retour sur un sytème de base amélioré:

```
# ./centos-setup.sh --strip
```

Affiche l'aide:

```
# ./centos-setup.sh --help
```

Et si vous voulez savoir ce qui se passe derrière, ouvrez une deuxieme console
et affichez les logs:
```
$ tail -f /tmp/centos-setup.log
```

