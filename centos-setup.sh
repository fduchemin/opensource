#!/bin/bash
#
# centos-setup.sh
#
# (c) Niki Kovacs 2020 <info@microlinux.fr>

# Enterprise Linux version
VERSION="el7"

# Dossier actuel
CWD=$(pwd)

# Utilisateur defini
USERS="$(ls -A /home)"

# Utilisateur administrateur
ADMIN=$(getent passwd 1000 | cut -d: -f 1)

# Désinstallation des packages du fichier
CRUFT=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/useless-packages.txt)

# Installation des packages du fichier
EXTRA=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/extra-packages.txt)

# Utilisateur defini
USERS="$(ls -A /home)"

# Mirroirs
ELREPO="https://elrepo.org/linux/elrepo/${VERSION}/x86_64/RPMS"
CISOFY="https://packages.cisofy.com"

# Log
LOG="/tmp/$(basename "${0}" .sh).log"
echo > ${LOG}

usage() {
  echo "Usage: ${0} OPTION"
  echo 'CentOS 7.x configuration post-installation.'
  echo 'Options:'
  echo '  -1, --shell    Configure le shell: Bash, Vim, console, etc.'
  echo '  -2, --repos    Configure les dépots officiels et tiers.'
  echo '  -3, --extra    Installe un système de base optimisé.'
  echo '  -4, --prune    Desinstalle les packages inutiles.'
  echo '  -5, --logs     Autorises les utilisateurs admin aux logs.'
  echo '  -6, --ipv4     Desactive IPv6 et reconfigure les services concernés.'
  echo '  -7, --sudo     Configure un mot de passe persistent pour sudo.'
  echo '  -8, --setup    Applique tous les éléments ci-dessus.'
  echo '  -9, --strip    Retour a un système de base optimisé.'
  echo '  -h, --help     Affiche ce message.'
  echo "Les LOGS sont stockés ${LOG}."
}

configure_shell() {
  # Personnalisation d'affichage des consoles.
  echo 'Configuration de Bash shell pour root.'
  cat ${CWD}/${VERSION}/bash/bashrc-root > /root/.bashrc
  echo 'Configuration de Bash shell pour les utilisateurs.'
  cat ${CWD}/${VERSION}/bash/bashrc-users > /etc/skel/.bashrc
  # Les utilisateurs actuels pourraient vouloir l'utiliser.
  if [ ! -z "${USERS}" ]
  then
    for USER in ${USERS}
    do
      cat ${CWD}/${VERSION}/bash/bashrc-users > /home/${USER}/.bashrc
      chown ${USER}:${USER} /home/${USER}/.bashrc
    done
  fi
  # Ajoutez une poignée d'options astucieuses pour Vim.
  echo 'Configuration de Vim.'
  cat ${CWD}/${VERSION}/vim/vimrc > /etc/vimrc
  # Configure l'anglais comme langue principale.
  echo 'Configuration de system locale.'
  localectl set-locale LANG=en_US.UTF8
  # Configure la resolution de la console
  if [ -f /boot/grub2/grub.cfg ]
  then
    echo 'Configuration de la résolution de la console.'
    sed -i -e 's/rhgb quiet/nomodeset quiet vga=791/g' /etc/default/grub
    grub2-mkconfig -o /boot/grub2/grub.cfg >> ${LOG} 2>&1
  fi
}

configure_repos() {
  # Active les dépots [base], [updates] et [extra] avec une priorité à 1.
  echo 'Configuration des dépots officiels.'
  cat ${CWD}/${VERSION}/yum/CentOS-Base.repo > /etc/yum.repos.d/CentOS-Base.repo
  sed -i -e 's/installonly_limit=5/installonly_limit=2/g' /etc/yum.conf
  # Active le dépot [cr] avec une priorité à 1.
  echo 'Configuration du dépot CR.'
  cat ${CWD}/${VERSION}/yum/CentOS-CR.repo > /etc/yum.repos.d/CentOS-CR.repo
  # Active le dépot [sclo] avec un priorité à 1.
  echo 'Configuration du dépot SCLo.'
  if ! rpm -q centos-release-scl > /dev/null 2>&1
  then
    yum -y install centos-release-scl >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/CentOS-SCLo-scl-rh.repo > /etc/yum.repos.d/CentOS-SCLo-scl-rh.repo
  cat ${CWD}/${VERSION}/yum/CentOS-SCLo-scl.repo > /etc/yum.repos.d/CentOS-SCLo-scl.repo
  # Active Delta RPM.
  if ! rpm -q deltarpm > /dev/null 2>&1
  then
    echo 'Installation de  Delta RPM.'
    yum -y install deltarpm >> ${LOG} 2>&1
  fi
  # Mise à jour
  echo 'Mise à jour initial.'
  echo 'Cela peut prendre un moment...'
  yum -y update >> ${LOG} 2>&1
  # Installe le plugin Yum-Priorities
  if ! rpm -q yum-plugin-priorities > /dev/null 2>&1
  then
    echo 'Installation du plugin Yum-Priorities.'
    yum -y install yum-plugin-priorities >> ${LOG} 2>&1
  fi
  # Active le dépot [epel] avec une priorité à 10.
  echo 'Configuration du dépot EPEL.' 
  if ! rpm -q epel-release > /dev/null 2>&1
  then
    yum -y install epel-release >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/epel.repo > /etc/yum.repos.d/epel.repo
  cat ${CWD}/${VERSION}/yum/epel-testing.repo > /etc/yum.repos.d/epel-testing.repo
  # Configure les dépots [elrepo] et [elrepo-kernel] sans les activer.
  echo 'Configuration des dépots ELRepo.'
  if ! rpm -q elrepo-release > /dev/null 2>&1
  then
    yum -y localinstall \
    ${ELREPO}/elrepo-release-7.0-4.${VERSION}.elrepo.noarch.rpm >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/elrepo.repo > /etc/yum.repos.d/elrepo.repo
  # Active le dépot [lynis] avec une priorité de 5.
  echo 'Configuration du dépot Lynis.'
  if [ ! -f /etc/yum.repos.d/lynis.repo ]
  then
    rpm --import ${CISOFY}/keys/cisofy-software-rpms-public.key >> ${LOG} 2>&1
  fi
  cat ${CWD}/${VERSION}/yum/lynis.repo > /etc/yum.repos.d/lynis.repo
}

install_extras() {
  echo 'Recuperation des packages manquant provenant Core.' 
  yum group mark remove "Core" >> ${LOG} 2>&1
  yum -y group install "Core" >> ${LOG} 2>&1
  echo 'Les packages Core sont instllés sur le systeme.'
  echo 'Installation des packages Base.'
  echo 'Ceci peut durer un moment...'
  yum group mark remove "Base" >> ${LOG} 2>&1
  yum -y group install "Base" >> ${LOG} 2>&1
  echo 'Les packages Base sont instllés sur le systeme.'
  echo 'Installation de packages additionnels'
  for PACKAGE in ${EXTRA}
  do
    if ! rpm -q ${PACKAGE} > /dev/null 2>&1
    then
      echo "Installion du package: ${PACKAGE}"
      yum -y install ${PACKAGE} >> ${LOG} 2>&1
    fi
  done
  echo 'Tous les packages additionnel sont installés sur le systeme.'
}

remove_cruft() {
  echo 'Suppression des packages inutiles.'
  for PACKAGE in ${CRUFT}
  do
    if rpm -q ${PACKAGE} > /dev/null 2>&1
    then
      echo "Suppression du package: ${PACKAGE}"
      yum -y remove ${PACKAGE} >> ${LOG} 2>&1
      if [ "${?}" -ne 0 ]
        then
        echo "Le packages ${PACKAGE} ne peut etre enleve." >&2
        exit 1
      fi
    fi
  done
  echo 'Tous les packages inutiles ont ete desinstalles.'
}

configure_logs() {
  # Les utilisateurs Admin ont accès aux logs du système
  if [ ! -z "${ADMIN}" ]
  then
    if getent group systemd-journal | grep ${ADMIN} > /dev/null 2>&1
    then
      echo "L'utilisateur admin ${ADMIN} est déja membre du groupe systemd-journal."
    else
      echo "Ajout de l'utilisateur admin ${ADMIN} au groupe systemd-journal."
      usermod -a -G systemd-journal ${ADMIN}
    fi
  fi
}

disable_ipv6() {
  # Désactive IPv6
  echo 'Desactivation de l'IPv6.'
  cat ${CWD}/${VERSION}/sysctl.d/disable-ipv6.conf > /etc/sysctl.d/disable-ipv6.conf
  sysctl -p --load /etc/sysctl.d/disable-ipv6.conf >> $LOG 2>&1
  # Reconfigure SSH 
  if [ -f /etc/ssh/sshd_config ]
  then
    echo 'Configuring SSH server for IPv4 only.'
    sed -i -e 's/#AddressFamily any/AddressFamily inet/g' /etc/ssh/sshd_config
    sed -i -e 's/#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/g' /etc/ssh/sshd_config
  fi
  # Reconfigure Postfix
  if [ -f /etc/postfix/main.cf ]
  then
    echo 'Configuring Postfix server for IPv4 only.'
    sed -i -e 's/inet_protocols = all/inet_protocols = ipv4/g' /etc/postfix/main.cf
    systemctl restart postfix
  fi
  # Reconstruit initrd
  echo 'Reconstruction ramdisk.'
  dracut -f -v >> $LOG 2>&1
}

configure_sudo() {
  # Configure une mot de passe persistent pour sudo.
  if grep timestamp_timeout /etc/sudoers > /dev/null 2>&1
  then
    echo 'Un mot de passe persistent pour sudo est deja en place.'
  else
    echo 'Configuration d'un mot de passe persistent pour sudo.'
    echo >> /etc/sudoers
    echo '# Timeout' >> /etc/sudoers
    echo 'Defaults timestamp_timeout=-1' >> /etc/sudoers
  fi
}

strip_system() {
  # Desinstalle tous les packages qui ne sont pas optimisés pour le système de
  # base.
  echo 'Stripping system.'
  local TMP='/tmp'
  local PKGLIST="${TMP}/pkglist"
  local PKGINFO="${TMP}/pkg_base"
  rpm -qa --queryformat '%{NAME}\n' | sort > ${PKGLIST}
  PACKAGES=$(egrep -v '(^\#)|(^\s+$)' $PKGLIST)
  rm -rf ${PKGLIST} ${PKGINFO}
  mkdir ${PKGINFO}
  unset REMOVE
  echo 'Creation de la base de donnée.'
  BASE=$(egrep -v '(^\#)|(^\s+$)' ${CWD}/${VERSION}/yum/enhanced-base.txt)
  for PACKAGE in ${BASE}
  do
    touch ${PKGINFO}/${PACKAGE}
  done
  for PACKAGE in ${PACKAGES}
  do
    if [ -r ${PKGINFO}/${PACKAGE} ]
    then
      continue
    else
      REMOVE="${REMOVE} ${PACKAGE}"
    fi
  done
  if [ ! -z "${REMOVE}" ]
  then
    for PACKAGE in ${REMOVE}
    do
      if rpm -q ${PACKAGE} > /dev/null 2>&1
      then
        echo "Desinstallation de: ${PACKAGE}"
        yum -y remove ${PACKAGE} >> ${LOG} 2>&1
      fi
    done
  fi
  configure_repos
  install_extras
  remove_cruft
  rm -rf ${PKGLIST} ${PKGINFO}
}

# Vérifie que le script est executé avec les privilèges admin.
if [[ "${UID}" -ne 0 ]]
then
  echo 'Executez ent tant que root ou sudo.' >&2
  exit 1
fi

# Vérifie les paramètres.
if [[ "${#}" -ne 1 ]]
then
  usage
  exit 1
fi
OPTION="${1}"
case "${OPTION}" in
  -1|--shell) 
    configure_shell
    ;;
  -2|--repos) 
    configure_repos
    ;;
  -3|--extra) 
    install_extras
    ;;
  -4|--prune) 
    remove_cruft
    ;;
  -5|--logs) 
    configure_logs
    ;;
  -6|--ipv4) 
    disable_ipv6
    ;;
  -7|--sudo) 
    configure_sudo
    ;;
  -8|--setup) 
    configure_shell
    configure_repos
    install_extras
    remove_cruft
    configure_logs
    disable_ipv6
    configure_sudo
    ;;
  -9|--strip) 
    strip_system
    ;;
  -h|--help) 
    usage
    exit 0
    ;;
  ?*) 
    usage
    exit 1
esac

exit 0

